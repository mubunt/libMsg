# RELEASE NOTES: *libMsg*, a program to display an error or any other information in the form of a text box in the middle of the user window.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.0.6**:
  - Updated build system components.

- **Version 1.0.5**:
  - Updated build system.

- **Version 1.0.4**:
  - Removed unused files.

- **Version 1.0.3**:
  - Updated build system component(s)

- **Version 1.0.2**:
  - Renamed executable built as example as *example_xxx* to be compliant with other projects.

- **Version 1.0.1**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.0.0**:
  - First version.
