//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libMsg
// A program to display an error or any other information in the form of a text
// box in the middle of the user window
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "libMsg.h"
//------------------------------------------------------------------------------
// AUTO-TEST MODE
//------------------------------------------------------------------------------
int main( void ) {
	const char *label1 = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor\n" \
	                     "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Sadipscing elitr,\n" \
	                     "sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n" \
	                     "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,\n"
	                     "no sea takimata sanctus est. Lorem ipsum dolor sit amet";

	fprintf(stdout, "1) Error message: %s\n", displayError(label1) ? "OK" : "ERROR");
	fprintf(stdout, "2) Message message: %s\n", displayMessage(label1) ? "OK" : "ERROR");

	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
