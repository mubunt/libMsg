 # *libMsg*, routines to display an error or any other information in a text box in the middle of the window.

The **libMsg** library provides two routines to display an error or any other information in the form of a text box in the middle of the user window:
- The content of the window is saved,
- The window is cleared,
- The message is displayed in the form of a text box in the middle of the window, in red if it is an error, if white   - herwise,
- The user's validation is expected,
- The window is cleared and the initial content is restored.

## EXAMPLES
- Display of an error:
![Error](README_images/libMsg01.png  "Example #1")
- Display of the message:
![Message](README_images/libMsg02.png  "Example #2")

## LICENSE
**libMsg** is covered by the GNU General Public License (GPL) version 3 and above.

## API DEFINITION
### displayError
*Declaration:*

```C
#include "libMsg.h"
bool displayError( const char *text );
```

*Description:*

*Return value:*
- true: all is fine.
- false: an issue is raised.

### displayMessage
*Declaration:*

```C
#include "libMsg.h"
bool displayMessage( const char *text );
```

*Description:*

*Return value:*
- true: all is fine.
- false: an issue is raised.

## STRUCTURE OF THE APPLICATION
This section walks you through **libMsg**'s structure. Once you understand this structure, you will easily find your way around in **libMsg**'s code base.

``` bash
./                    # Application level
├── README_images/    # 
│   ├── libMsg01.png  # 
│   └── libMsg02.png  # 
├── examples/         # 
│   ├── Makefile      # Makefile
│   └── basic.c       # Example of 'libMsg' usage
├── src/              # Source directory
│   ├── Makefile      # Makefile
│   ├── libMsg.c      # Source file
│   ├── libMsg.c.orig # 
│   └── libMsg.h      # Header file to be included in user's program
├── COPYING.md        # GNU General Public License markdown file
├── LICENSE.md        # License markdown file
├── Makefile          # Makefile
├── README.md         # ReadMe markdown file
├── RELEASENOTES.md   # Release Notes markdown file
└── VERSION           # Version identification text file

3 directories, 14 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd libMsg
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd libMsg
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## HOW TO PLAY WITH THIS APPLICATION
```Shell
$ cd libMsg
$ make all
$ ./linux/msg_example
```

## SOFTWARE REQUIREMENTS
- For usage and development, nothing particular...
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***