//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libMsg
// A program to display an error or any other information in the form of a text
// box in the middle of the user window
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <termcap.h>
#include <termios.h>
#include <stdarg.h>
#include <stdbool.h>
#include <sys/ioctl.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "libMsg.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define DISPLAY(...)						fprintf(stdout, __VA_ARGS__)
#define FLUSH()								fflush(stdout)
#define SAVEINITSCREEN(n)					do { \
												tputs(screeninit, (int)n, putchar); \
												_screeninit = true; \
											} while (0)
#define RESTOREINITSCREEN(n)				if (_screeninit) { \
												tputs(screendeinit, (int)n, putchar); \
											}
#define MOVE_CURSOR(line, column)			tputs(tgoto(move, ((int)(column - 1) % nbCOLS), (int)(line - 1)), 1, putchar)
#define HIDE_CURSOR()						DISPLAY("\033[?25l")
#define SHOW_CURSOR()						DISPLAY("\033[?25h")
#define DISPLAY_BELL()						do { \
												DISPLAY("\033[?5h"); FLUSH(); \
												usleep(50000); \
												DISPLAY("\033[?5l"); FLUSH(); \
											} while (0)

#define MAX(a,b)							(a > b ? a : b)

#define BOLDRED_COLOR						"\033[1;31m"
#define BOLDWHITE_COLOR						"\033[1;37m"
#define ITALIC								"\033[3m"
#define STOP_COLOR							"\033[0m"

#define ERRORPREFIX							"LibMsg ERROR: "

#define ENTER 								0x0d

#define GDUL 								"╔"
#define GDUR 								"╗"
#define GDBL 								"╚"
#define GDBR 								"╝"
#define GDV 								"║"
#define GDH 								"═"
#define GDVL 								"╟"
#define GDVR								"╢"
#define GSH 								"─"
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum { MODE_ECHOED, MODE_RAW }		terminal;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static const char 	*error_label			= "APPLICATION ERROR";
static const char 	*message_label			= "APPLICATION MESSAGE";
static const char 	*press_label			= "Press 'enter' to go back to application.";

static char 		*screeninit;			// Startup terminal initialization
static char 		*screendeinit;			// Exit terminal de-initialization
static char 		*clear;					// Clear screen
static char 		*move;					// Cursor positioning
static bool 		_screeninit				= false;

static int 			nbLINES 				= 0;
static int 			nbCOLS 					= 0;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static bool _fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "\n" BOLDRED_COLOR "%s%s. Abort!!!" STOP_COLOR "\n\n", ERRORPREFIX, buff);
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _setTerminalMode( terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &cooked))
			return(_fatal("%s", "Cannot set the parameter 'MODE_ECHOED' associated with the terminal"));
		break;
	case MODE_RAW:
		if (0 != tcgetattr(STDIN_FILENO, &cooked))
			return(_fatal("%s", "Cannot set the parameter 'MODE_RAW' associated with the terminal"));
		raw = cooked;
		cfmakeraw(&raw);
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &raw))
			return(_fatal("%s", "Cannot set the parameter 'MODE_RAW/TCSANOW' associated with the terminal"));
		break;
	}
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _getTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		return(_fatal("%s", "The terminfo database could not be found"));
	case 0:
		return(_fatal("%s", "There is no entry for this terminal in the terminfo database"));
	default:
		break;
	}
	// Get various string-valued capabilities.
	static char sbuf[TERMSBUF_SIZE];
	char *sp = sbuf;
	if (NULL == (screeninit = tgetstr("ti", &sp)))
		return(_fatal("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database"));
	if (NULL == (screendeinit = tgetstr("te", &sp)))
		return(_fatal("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database"));
	if (NULL == (clear = tgetstr("cl", &sp)))
		return(_fatal("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database"));
	if (NULL == (move = tgetstr("cm", &sp)))
		return(_fatal("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database"));
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _getTerminalSize( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		return(_fatal("%s", "Cannot get terminal size"));
	*lines = w.ws_row;
	*cols = w.ws_col;
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *_processLabel( const char *text, int *nblines, int *lengthline ) {
	char *ptres = strdup(text);
	char *pt = ptres;
	char *pstart_line = ptres;
	*lengthline = 0;
	*nblines = 0;
	while (*pt != '\0') {
		if (*pt == '\n') {
			++*nblines;
			*pt = '\0';
			*lengthline = MAX(*lengthline, (int)strlen(pstart_line));
			pstart_line = pt + 1;
		}
		++pt;
	}
	if (strlen(pstart_line) != 0) {
		++*nblines;
		*lengthline = MAX(*lengthline, (int)strlen(pstart_line));
	}
	return ptres;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _readKey( void ) {
	while (1) {
		if (ENTER == getchar()) break;
		DISPLAY_BELL();
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _display( const char *label, const char *title, const char *color ) {
	if (! _getTerminalCapabilities()) return false;
	if (! _getTerminalSize(&nbLINES, &nbCOLS)) return false;

	int nbl, lenl;
	char *text = _processLabel(label, &nbl, &lenl);
	int width = MAX(lenl, (int)strlen(press_label));
	if (width > nbCOLS -4)
		return(_fatal("Message too long"));
	// The heigh of the display is the number of lines of the label, plus
	// edges (2) + 2 lines for title + 2 lines for "press_label" message
	int nbltot = nbl + 6;
	if (nbltot >= nbLINES)
		return(_fatal("Too many lines (need %d lines for %d available)", nbltot, nbLINES));
	// Position of the display
	int x = (nbLINES - nbltot) / 2;
	int y = (nbCOLS - (int)width) / 2;

	if (! _setTerminalMode(MODE_RAW)) return false;
	SAVEINITSCREEN(nbLINES);
	HIDE_CURSOR();

	MOVE_CURSOR(x++, y);
	DISPLAY("%s%s", color, GDUL);
	for (int _i = 0; _i < width + 2; _i++) DISPLAY(GDH);
	DISPLAY("%s" STOP_COLOR, GDUR);

	MOVE_CURSOR(x++, y);
	DISPLAY("%s%s", color, GDV);
	int w = width - (int)strlen(title);
	DISPLAY(" %*s%s%*s", w / 2, " ", title, (w - w / 2), " ");
	DISPLAY(" %s" STOP_COLOR, GDV);

	MOVE_CURSOR(x++, y);
	DISPLAY("%s%s", color, GDVL);
	for (int i = 0; i < width + 2; i++) DISPLAY(GSH);
	DISPLAY("%s" STOP_COLOR, GDVR);

	char *pttext = text;
	for (int i = 0; i < nbl; i++) {
		MOVE_CURSOR(x++, y);
		DISPLAY("%s%s ", color, GDV);
		DISPLAY("%-*s", width, pttext);
		DISPLAY(" %s" STOP_COLOR, GDV);
		pttext = pttext + strlen(pttext) + 1;
	}

	MOVE_CURSOR(x++, y);
	DISPLAY("%s%s", color, GDV);
	for (int i = 0; i < width + 2; i++) DISPLAY(" ");
	DISPLAY("%s" STOP_COLOR, GDV);

	MOVE_CURSOR(x++, y);
	DISPLAY(ITALIC "%s%s ", color, GDV);
	w = width - (int)strlen(press_label);
	DISPLAY("%*s", width, press_label);
	DISPLAY(" %s" STOP_COLOR, GDV);

	MOVE_CURSOR(x++, y);
	DISPLAY("%s%s", color, GDBL);
	for (int i = 0; i < width + 2; i++) DISPLAY(GDH);
	DISPLAY("%s" STOP_COLOR, GDBR);

	FLUSH();

	free(text);
	_readKey();

	SHOW_CURSOR();
	RESTOREINITSCREEN(nbLINES);
	_setTerminalMode(MODE_ECHOED);
	FLUSH();
	return true;
}
//------------------------------------------------------------------------------
// LIBRARY FUNCTION
//------------------------------------------------------------------------------
bool displayError( const char *label ) {
	return(_display(label, error_label, BOLDRED_COLOR));
}

bool displayMessage( const char *label ) {
	return(_display(label, message_label, BOLDWHITE_COLOR));
}
//------------------------------------------------------------------------------
